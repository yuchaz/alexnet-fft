from scipy.misc import imread
from scipy.misc import imresize
import numpy as np
import os
import math
from skimage.measure import compare_ssim as ssim
import matplotlib.pyplot as plt

def img_read(filename):
    img = (imread( os.path.join('./image', filename) )[:,:,:3]).astype(np.float32)
    img = img - np.mean(img)
    img[:, :, 0], img[:, :, 2] = img[:, :, 2], img[:, :, 0]
    return img

def load_alexnet_weights():
    return np.load(open("bvlc_alexnet.npy", "rb"), encoding="latin1").item()

def calc_mean_square_error(true_val, pred_val):
    image_size = true_val.shape[1]
    return np.sum(np.square(true_val - pred_val), axis=(1,2)) / (image_size**2)

def calc_ssim(true_val, pred_val):
    samples, img_shape, is_, channels = true_val.shape
    ssim_lists = np.zeros((samples,channels))
    for i in range(samples):
        for j in range(channels):
            ssim_lists[i,j] = ssim(true_val[i,:,:,j],pred_val[i,:,:,j])
    return ssim_lists

def calc_mse_psnr_ssim(true_val, pred_val):
    mse = np.mean(calc_mean_square_error(true_val,pred_val))
    psnr = 10*math.log(255**2/mse, 10)
    ssim = np.mean(calc_ssim(true_val,pred_val))
    return mse, psnr, ssim

def plotNNFilter(units, filename, layer_name, input_type="figure"):
    filters = units.shape[3]
    plt.figure(figsize=(100,100))
    n_columns = 10
    n_rows = math.ceil(filters / n_columns) + 1
    for i in range(filters):
        plt.subplot(n_rows, n_columns, i+1)
        if input_type == "figure":
            plt.imshow(units[0,:,:,i], interpolation="nearest", cmap="gray")
        elif input_type == "weight":
            plt.imshow(units[:,:,0,i], interpolation="nearest", cmap="gray")
        else:
            raise ("You should only input type either be figure or weight")
    plt.savefig("./layer_activation/{}_{}.png".format(layer_name, filename.split('.')[0]))
