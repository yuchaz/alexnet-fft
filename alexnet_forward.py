################################################################################
#
# Codes built upon Michael Guerzhoy and Davi Frossard's work.
# See details: http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/
#
################################################################################

from numpy import *
import tensorflow as tf
from caffe_classes import class_names
import argparse
import utils
from scipy.signal import convolve2d
import math

cli_description = \
"""Command line interface for alexnet forward.
Work built upon Michael Guerzhoy and Davi Frossard's work.
Details can be found here: http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/"""

parser = argparse.ArgumentParser(description=cli_description)

################################################################################
# Control which part of the program will run, a.k.a. "action tags".
# Should only use one of them each time.
# If nothing specify, will execute runClassifier(), which is forward pass of AlexNet.
parser.add_argument('--get-specific-layer', '-sl', type=str, nargs=1,
    help="specify when want to get specific layer to activate.")
parser.add_argument('--comp-fft-conv', '-co', nargs='?', default=False, const=True,
    help="Specify when comparing FFT and CONV output on first layer output")
parser.add_argument('--get-all-activations', '-aa', nargs='?', default=False, const=True,
    help="Specify when would like to get all activations.")

################################################################################
# Control the main images, im1.
parser.add_argument('--img-visualize', '-iv', type=str, default="laska.png",
    help="The image to visualize.")
# Choose whether to run debug mode.
# In such mode, breakpoint will be triggered when encounter an error.
parser.add_argument('--debug', '-D', nargs='?', default=False, const=True,
    help="Specify when running debug mode.")

################################################################################

args = parser.parse_args()

train_x = zeros((1, 227,227,3)).astype(float32)
train_y = zeros((1, 1000))
xdim = train_x.shape[1:]
ydim = train_y.shape[1]

################################################################################
# Read Image, and change to BGR

im1 = utils.img_read(args.img_visualize)
im2 = utils.img_read("poodle.png")
img_visualize = utils.img_read(args.img_visualize)

################################################################################
# Network Architectures areas.
#
# Authors:
# Michael Guerzhoy and Davi Frossard, 2016
# AlexNet implementation in TensorFlow, with weights
# Details:
# http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/
#
# With code from https://github.com/ethereon/caffe-tensorflow
# Model from  https://github.com/BVLC/caffe/tree/master/models/bvlc_alexnet
# Weights from Caffe converted using https://github.com/ethereon/caffe-tensorflow

net_data = utils.load_alexnet_weights()

def conv(input, kernel, biases, k_h, k_w, c_o, s_h, s_w,  padding="VALID", group=1):
    '''From https://github.com/ethereon/caffe-tensorflow
    '''
    c_i = input.get_shape()[-1]
    assert c_i%group==0
    assert c_o%group==0
    convolve = lambda i, k: tf.nn.conv2d(i, k, [1, s_h, s_w, 1], padding=padding)


    if group==1:
        conv = convolve(input, kernel)
    else:
        input_groups =  tf.split(input, group, 3)   #tf.split(3, group, input)
        kernel_groups = tf.split(kernel, group, 3)  #tf.split(3, group, kernel)
        output_groups = [convolve(i, k) for i,k in zip(input_groups, kernel_groups)]
        conv = tf.concat(output_groups, 3)          #tf.concat(3, output_groups)
    return  tf.reshape(tf.nn.bias_add(conv, biases), [-1]+conv.get_shape().as_list()[1:])

x = tf.placeholder(tf.float32, (None,) + xdim)
# x = [~, 227, 227, 3]


#conv1
#conv(11, 11, 96, 4, 4, padding='VALID', name='conv1')
k_h = 11; k_w = 11; c_o = 96; s_h = 4; s_w = 4
conv1W = tf.Variable(net_data["conv1"][0])
# import pdb; pdb.set_trace()
conv1b = tf.Variable(net_data["conv1"][1])
conv1_in = conv(x, conv1W, conv1b, k_h, k_w, c_o, s_h, s_w, padding="SAME", group=1)
conv1 = tf.nn.relu(conv1_in)

#lrn1
#lrn(2, 2e-05, 0.75, name='norm1')
radius = 2; alpha = 2e-05; beta = 0.75; bias = 1.0
lrn1 = tf.nn.local_response_normalization(conv1,
                                          depth_radius=radius,
                                          alpha=alpha,
                                          beta=beta,
                                          bias=bias)

#maxpool1
#max_pool(3, 3, 2, 2, padding='VALID', name='pool1')
k_h = 3; k_w = 3; s_h = 2; s_w = 2; padding = 'VALID'
maxpool1 = tf.nn.max_pool(lrn1, ksize=[1, k_h, k_w, 1], strides=[1, s_h, s_w, 1], padding=padding)

#conv2
#conv(5, 5, 256, 1, 1, group=2, name='conv2')
k_h = 5; k_w = 5; c_o = 256; s_h = 1; s_w = 1; group = 2
conv2W = tf.Variable(net_data["conv2"][0])
conv2b = tf.Variable(net_data["conv2"][1])
# import pdb; pdb.set_trace()
conv2_in = conv(maxpool1, conv2W, conv2b, k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
conv2 = tf.nn.relu(conv2_in)


#lrn2
#lrn(2, 2e-05, 0.75, name='norm2')
radius = 2; alpha = 2e-05; beta = 0.75; bias = 1.0
lrn2 = tf.nn.local_response_normalization(conv2,
                                                  depth_radius=radius,
                                                  alpha=alpha,
                                                  beta=beta,
                                                  bias=bias)

#maxpool2
#max_pool(3, 3, 2, 2, padding='VALID', name='pool2')
k_h = 3; k_w = 3; s_h = 2; s_w = 2; padding = 'VALID'
maxpool2 = tf.nn.max_pool(lrn2, ksize=[1, k_h, k_w, 1], strides=[1, s_h, s_w, 1], padding=padding)

#conv3
#conv(3, 3, 384, 1, 1, name='conv3')
k_h = 3; k_w = 3; c_o = 384; s_h = 1; s_w = 1; group = 1
conv3W = tf.Variable(net_data["conv3"][0])
conv3b = tf.Variable(net_data["conv3"][1])
conv3_in = conv(maxpool2, conv3W, conv3b, k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
conv3 = tf.nn.relu(conv3_in)

#conv4
#conv(3, 3, 384, 1, 1, group=2, name='conv4')
k_h = 3; k_w = 3; c_o = 384; s_h = 1; s_w = 1; group = 2
conv4W = tf.Variable(net_data["conv4"][0])
conv4b = tf.Variable(net_data["conv4"][1])
conv4_in = conv(conv3, conv4W, conv4b, k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
conv4 = tf.nn.relu(conv4_in)


#conv5
#conv(3, 3, 256, 1, 1, group=2, name='conv5')
k_h = 3; k_w = 3; c_o = 256; s_h = 1; s_w = 1; group = 2
conv5W = tf.Variable(net_data["conv5"][0])
conv5b = tf.Variable(net_data["conv5"][1])
conv5_in = conv(conv4, conv5W, conv5b, k_h, k_w, c_o, s_h, s_w, padding="SAME", group=group)
conv5 = tf.nn.relu(conv5_in)

#maxpool5
#max_pool(3, 3, 2, 2, padding='VALID', name='pool5')
k_h = 3; k_w = 3; s_h = 2; s_w = 2; padding = 'VALID'
maxpool5 = tf.nn.max_pool(conv5, ksize=[1, k_h, k_w, 1], strides=[1, s_h, s_w, 1], padding=padding)

#fc6
#fc(4096, name='fc6')
fc6W = tf.Variable(net_data["fc6"][0])
fc6b = tf.Variable(net_data["fc6"][1])
fc6 = tf.nn.relu_layer(tf.reshape(maxpool5, [-1, int(prod(maxpool5.get_shape()[1:]))]), fc6W, fc6b)

#fc7
#fc(4096, name='fc7')
fc7W = tf.Variable(net_data["fc7"][0])
fc7b = tf.Variable(net_data["fc7"][1])
fc7 = tf.nn.relu_layer(fc6, fc7W, fc7b)

#fc8
#fc(1000, relu=False, name='fc8')
fc8W = tf.Variable(net_data["fc8"][0])
fc8b = tf.Variable(net_data["fc8"][1])
fc8 = tf.nn.xw_plus_b(fc7, fc8W, fc8b)


#prob
#softmax(name='prob'))
prob = tf.nn.softmax(fc8)

init = tf.initialize_all_variables()
sess = tf.Session()
sess.run(init)

################################################################################
# comparison between fft and conv, will run when --comp-fft-conv flag is specified
def calc_fft(input_d, convW, convb, strip_size, to256=False):
    # Compute convolution based on FFT.
    img_shape = input_d.shape[1] if not to256 else 256
    orig_img_shape = input_d.shape[1]
    convW_shape = convW.shape[0]
    full_size = img_shape + convW_shape - 1
    i_pad_front = int((full_size - orig_img_shape) / 2)
    i_pad_back = full_size - orig_img_shape - i_pad_front
    images_pad = pad(input_d, [[0,0],[i_pad_front,i_pad_back],[i_pad_front,i_pad_back],[0,0]], "constant", constant_values=0)
    intput_fft2d = fft.rfftn(images_pad, axes=(1,2))

    w_pad_front = int((full_size - convW_shape) / 2)
    w_pad_back = full_size - convW_shape - w_pad_front
    convW_pad = pad(convW, [[w_pad_front,w_pad_back],[w_pad_front,w_pad_back],[0,0],[0,0]], "constant", constant_values=0)
    convW_fft2d = fft.rfftn(convW_pad,axes=(0,1))

    convolved = einsum('ijkl,jklm->ijklm', intput_fft2d, convW_fft2d)
    convolved_add_b = convolved + convb
    convolved_sum = sum(convolved_add_b,axis=3)
    fft_output_layer = fft.fftshift(fft.irfftn(convolved_sum,axes=(1,2)), axes=(1,2)).real
    fft_output_layer_relu = maximum(fft_output_layer, 0, fft_output_layer)
    # valid_start = convW_shape - 1
    # valid_end = valid_start + img_shape - convW_shape + 1
    valid_start = math.floor(convW_shape / 2) - 1
    if to256:
        valid_start += math.floor((256 - orig_img_shape) / 2)
    valid_end = valid_start + orig_img_shape - 1
    return fft_output_layer_relu[:,valid_start:valid_end:strip_size,valid_start:valid_end:strip_size,:], \
           intput_fft2d.real, convW_fft2d.real

def calc_conv(input_d,convW, convb, strip_size):
    # Compute convolution in naive way.
    # size of input d = -1, 227, 227, 3
    sample_size, image_size, is_, channel_in = input_d.shape
    # size of conv W = 11, 11, 3, 96
    kernel_size, ks_, ci_, channel_out = convW.shape

    output_conv = zeros((sample_size, image_size, image_size, channel_in, channel_out))
    for idx_sam, sample in enumerate(input_d):
        for idx_channel_in in range(channel_in):
            for idx_channel_out in range(channel_out):
                output_conv[idx_sam,:,:,idx_channel_in,idx_channel_out] = \
                    convolve2d(sample[:,:,idx_channel_in], convW[:,:,idx_channel_in,idx_channel_out], mode='same')

    # get output_conv
    output_conv_add_b = output_conv + convb
    output_conv_reduced = sum(output_conv_add_b, axis=3)
    output_conv_relu = maximum(output_conv_reduced, 0, output_conv_reduced)

    return output_conv_relu[:,0:output_conv.shape[2]:strip_size,0:output_conv.shape[2]:strip_size,:]

def generate_all_fft_images():
    run_fft(x,conv1W,conv1b,4, "conv1")

def run_fft(input_tensor,conv_weight_tensor,conv_b_tensor,strip_size,layer_name):
    output = sess.run([input_tensor,conv_weight_tensor,conv_b_tensor, conv1],feed_dict={x:[im1]})
    fft_out_img,input_fft,weight_fft = calc_fft(output[0],output[1],output[2],strip_size)
    fft_out_img_pad,input_fft_pad,weight_fft_pad = calc_fft(output[0],output[1],output[2],strip_size,to256=True)
    conv_out_img = calc_conv(output[0],output[1],output[2],strip_size)

    mse_tf, psnr_tf, ssim_tf = utils.calc_mse_psnr_ssim(output[3].astype(float64),fft_out_img)
    print ("\nQuality measures of tensorflow and fft")
    print ("Mean square error is:\t\t{}".format(mse_tf))
    print ("Peak signal-to-noise ratio is:\t{}".format(psnr_tf))
    print ("SSIM is:\t\t\t{}".format(ssim_tf))

    mse, psnr, ssim = utils.calc_mse_psnr_ssim(conv_out_img,fft_out_img)
    print ("\nQuality measures of naive convolution and fft")
    print ("Mean square error is:\t\t{}".format(mse))
    print ("Peak signal-to-noise ratio is:\t{}".format(psnr))
    print ("SSIM is:\t\t\t{}".format(ssim))

    mse_pad, psnr_pad, ssim_pad = utils.calc_mse_psnr_ssim(conv_out_img,fft_out_img_pad)
    print ("\nQuality measures of naive convolution and fft padded to size 256")
    print ("Mean square error is:\t\t{}".format(mse_pad))
    print ("Peak signal-to-noise ratio is:\t{}".format(psnr_pad))
    print ("SSIM is:\t\t\t{}".format(ssim_pad))

    utils.plotNNFilter(fft_out_img,'fft_output.fft',layer_name=layer_name)
    utils.plotNNFilter(fft_out_img_pad,'padded_fft_output.fft',layer_name=layer_name)
    utils.plotNNFilter(conv_out_img,'naive_conv_output.sconv',layer_name=layer_name)

################################################################################
# Basic AlexNet forward pass. Will run when no action tags are specified
def runClassifier():
    output = sess.run(prob, feed_dict = {x:[im1,im2]})
    for input_im_ind in range(output.shape[0]):
        inds = argsort(output)[input_im_ind,:]
        print("Image", input_im_ind)
        for i in range(5):
            print(class_names[inds[-1-i]], output[input_im_ind, inds[-1-i]])

################################################################################
# Produce one layer activations. Will run --get-specific-layer tag is specified.
# Layer name must follows this tag. E.g. --get-specific-layer conv1
def getActivations(layer_name,stimuli,filename):
    units = sess.run(eval(layer_name),feed_dict={x:[stimuli]})
    utils.plotNNFilter(units, filename, layer_name=layer_name)

################################################################################
# Produce all layer activations. Will run when --get-all-activations tag is specified
def getAllActivations(stimuli,filename):
    layernames = ['conv1','conv2','conv3','conv4','conv5','maxpool1','maxpool2','maxpool5']
    all_units = sess.run([ eval(ly_name) for ly_name in layernames ], feed_dict={x:[stimuli]})
    for units,layer_name in zip(all_units,layernames):
        utils.plotNNFilter(units,filename,layer_name=layer_name)

################################################################################
# Command line interface configuration area.

def debug_mode(main_func):
    def run_main():
        if not args.debug:
            main_func()
        else:
            try:
                main_func()
            except:
                import sys, pdb, traceback
                type, value, tb = sys.exc_info()
                traceback.print_exc()
                pdb.post_mortem(tb)
    return run_main

@debug_mode
def main():
    if args.comp_fft_conv:
        generate_all_fft_images()
    elif args.get_all_activations:
        getAllActivations(im1, args.img_visualize)
    elif args.get_specific_layer:
        getActivations(args.get_specific_layer[0],im1,args.img_visualize)
    else:
        runClassifier()

if __name__ == '__main__':
    main()
