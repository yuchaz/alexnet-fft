## Command line interface usages
```
python alexnet_forward.py [action_tags] [--img-visualize img_filename] [--debug]
```
`action_tags` can be either `--get-specific-layer`, `--comp-fft-conv` or `--get-all-activations`. Should only specify one each time. If nothing specified, classifier (AlexNet forward) will be executed. When using `--get-specific-layer` tag, need to provide exactly one layer to get.

Type `python alexnet_forward --help` for details.

## Packages used:
1. numpy
1. tensorflow
1. scipy
1. skimage
1. matplotlib

Install them before running this program.

## Notes:
1. Works built upon Michael Guerzhoy and Davi Frossard's work. Details in http://www.cs.toronto.edu/~guerzhoy/tf_alexnet/.
1. bvlc_alexnet.py and bvlc_alexnet.npy are generated using https://github.com/ethereon/caffe-tensorflow by converting the AlexNet weights/model from here: https://github.com/BVLC/caffe/tree/master/models/bvlc_alexnet
